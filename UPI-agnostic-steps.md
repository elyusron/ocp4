# Install DNS
- install DNS (can be installed in bastion node) (bind, powerdns, dnsmasq, F5big-ip(license))
- configure DNS (A + PTR records)
    - disable IPv6 by adding OPTIONS="-4" in /etc/sysconfig/named 
    - or in ubuntu/debian adding OPTIONS="-u bind -4" in /etc/default/named
    -  make sure all named conf files are owned by the user and permission to 755
# Install Load Balancer
- install LB (can be installed in bastion node) (HAProxy, nginx, ...)
- configure LB
- validating DNS lookup
- validating reverse DNS lookup


# Prepare bastion node
- download ocp installer from console.redhat.com or GitHub OKD
- download pull secret from console.redhat.com
- install oc cli
- change timezone of bastion to UTC
```
sudo timedatectl set-timezone UTC
```
- generate ssh-keygen in bastion node
```
ssh-keygen -t rsa -b 2048 -C "<email>"
```
- create install-config.yaml (add proxy if any)
- backup install-config.yaml to local computer
- generate kubernes manifest
```
./openshift-install create manifests --dir <installation_directory> 
```
- strong recommend: change status mastersSchedulable to false in
```
<installation_directory>/manifests/cluster-scheduler-02-config.yml
```
- optional if want to create separate partion on /var:
    - create butane config
    - create manifest using butane command
    - copy yaml file to <installation_directory>/openshift directory
- optional if want to NTP server for each host / node:
    - create butane config
    - create yaml using butane command
    - copy yaml file to <installation_directory>/openshift directory
- if OVNKubernetes is used for CNI Plugin
    - add cluster-network-03-config.yml in <installation_directory>/manifests/ directory
- generate ignition config files
```
./openshift-install create ignition-configs --dir <installation_directory> 
```
- install apache / nginx server
- upload all ignition config files to apache / nginx server


# Install RHCOS on each nodes (bootstrap, master; if success, continue to worker):
- set VMWare disk.EnableUUID to true for all nodes via vCenter GUI
- optional: generate sha512sum per .ign files in bastion node
- boot RHCOS ISO file (Liver version) from vCenter
- run sudo nmcli or nmtui to set static IP
- set static IP, DNS, gateway, disable IPv6
- check hostname
- make sure timezone of each node to UTC
- validating DNS lookup
```
dig api-int.labdev.ist.id
```
- validating reverse DNS lookup
```
dig +noall +answer @<dns_ip> -x ip_target
```
- validating internet access (redhat, etc)
```
dig quay.io
```
- example of test curl ignition config file
```
curl -o http://<apache_ip>:8080/directory/bootstrap.ign
sudo coreos-installer install -i ./bootstrap.ign /dev/sda -n
```
```
sudo coreos-installer install --copy-network --ignition-url=http://apache-IP:80/installation_directory/bootstrap.ign /dev/sda --insecure-ignition --ignition-hash=SHA512-a5a2d43879223273c9b60af66b44202a1d1248fc01cf156c46d4a79f552b6bad47bc8cc78ddf0116e80c59d2ea9e32ba53bc807afbca581aa059311def2c3e3b
```
- or
```
sudo coreos-installer install --copy-network --ignition-url=http://apache-IP:80/installation_directory/bootstrap.ign /dev/sda --insecure
```
- repeat for bootstrap & all master
If success, continue to worker
- shutdown VM and remove cd


# Install RH OCP:
- restart bootstrap, wait till up in LB (green color)
- restart master1,2,3, wait till up in LB (green color)
- restart worker1,2,3, wait till up in LB (green color)


## Monitor Bootstraping process:
- in bastion, run command:
```
./openshift-install --dir <installation_directory> wait-for bootstrap-complete --log-level=info 
journalctl -b -f -u release-image -u bootkube -u kubelet -u crio
```
- after bootstraping complete, you may remove boostrap node from LB

## Throubleshooting if any
```
systemctl status kubelet
systemctl status crio
```

## Logging to Cluster usin CLI
- export kubeadmin credentials: 
```
export KUBECONFIG=<installation_directory>/auth/kubeconfig
```
- run command: oc whoami


## Approve Certificate Signing Request:
- run command: 
```
oc get csr
```
- approve single by: 
```
oc adm certificate approve <csr_name>
```
- approve all by: 
```
oc get csr -o go-template='{{range .items}}{{if not .status}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}' | xargs --no-run-if-empty oc adm certificate approve
```
- run: 
```
oc get nodes
```


## Initial Operator Config:
- run: 
```
watch -n5 oc get clusteroperators
```
- Registry Storage Config:
```
oc get pod -n openshift-image-registry
oc edit configs.imageregistry.operator.openshift.io (leave it blank)
oc get clusteroperator image-registry
oc edit configs.imageregistry/cluster
```
- change line: managementState: Removed to managementState: Managed


## Completing Installation:
```
watch -n5 oc get clusteroperators
./openshift-install --dir <installation_directory> wait-for install-complete 
oc get pods --all-namespaces
oc logs <pod_name> -n namespace
```

# Next Step:
- Customize the cluster
- Set up anda config registry storage






