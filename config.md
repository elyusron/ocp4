create infrastructure machineSet (https://docs.openshift.com/container-platform/4.9/post_installation_configuration/cluster-tasks.html#post-install-creating-infrastructure-machinesets-production)
oc get machinesets -n openshift-machine-api
oc get machineset <machineset_name> -n openshift-machine-api -o yaml
create <file_name>.yaml
oc create -f <file_name>.yaml
oc get machineset -n openshift-machine-api

create infrastructure node
Add a label to the worker node: oc label node <node-name> node-role.kubernetes.io/app=""
Add a label to the infra nodes: oc label node <node-name> node-role.kubernetes.io/infra=""
oc get nodes
create node selector: 
edit scheduler: oc edit scheduler cluster
